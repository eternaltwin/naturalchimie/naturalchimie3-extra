function _setPnj(s)
{
   var _loc2_ = s.split(";");
   apply(this,_loc2_);
}
function apply(mc, infos)
{
   for(var _loc5_ in mc)
   {
      var _loc1_ = mc[_loc5_];
      if(typeof _loc1_ == "movieclip")
      {
         var _loc3_ = "smc";
         if(_loc1_._name == _loc3_)
         {
            _loc1_.gotoAndStop(infos[0]);
            infos.shift();
            if(infos.length > 0)
            {
               apply(_loc1_,infos);
            }
         }
      }
   }
}
