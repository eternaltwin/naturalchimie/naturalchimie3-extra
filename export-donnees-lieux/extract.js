var fs = require('fs');
var haxeTest = require('./HaxeTest');
var codec = require('./DataCodec');

const transformFileToSerialized = () => (filename, mode = "url") =>
{
	let init = fs.readFileSync(`${filename}.txt`, 'utf8');
	var split_input = init.split('&');

	let key = split_input[3].substring(2);
    let data = split_input[0].substring(2);
    if (mode === "url")
        data = decodeURIComponent(split_input[0].substring(2));
    if (mode === "base64")
        data = Buffer.from(split_input[0].substring(2), 'base64').toString();

    let result = codec.codecDeserialize(key, data)
    

    let jsonResult = haxeTest.Unserialize(result);

    fs.writeFileSync(`out/${filename}.json`, jsonResult);
	fs.writeFileSync(`out/${filename}-chain.json`, JSON.stringify(JSON.parse(jsonResult)['_chain']));
	fs.writeFileSync(`out/${filename}-weights.json`, JSON.stringify(JSON.parse(jsonResult)['_chWeight']));
	fs.writeFileSync(`out/${filename}-artifacts.json`, JSON.stringify(JSON.parse(jsonResult)['_artefacts']));


    
}

const runTransformFileToSerialized = transformFileToSerialized();

runTransformFileToSerialized("data");